﻿using Matrix_Calculator.Base;
using System;

namespace Matrix_Calculator.Services
{
    public class MatrixServices
    {
        public Matrix Transpose(Matrix matrix)
        {
            int rowCount = matrix.Elements.GetLength(0);
            int columnCount = matrix.Elements.GetLength(1);
            double[,] transposedElements = new double[columnCount, rowCount];

            for (int i = 0; i < columnCount; i++)
                for (int j = 0; j < rowCount; j++) { transposedElements[i, j] = matrix.Elements[j, i]; }

            return new Matrix(transposedElements);
        }

        public Matrix ScalarMultiply(double lambda, Matrix matrix)
        {
            double[,] elements = new double[matrix.RowCount, matrix.ColumnCount];
            for (int i = 0; i < matrix.RowCount; i++)
                for (int j = 0; j < matrix.ColumnCount; j++)
                {
                    elements[i, j] = matrix.Elements[i, j] * lambda;
                }
            return new Matrix(elements);
        }

 
 
    }
}
