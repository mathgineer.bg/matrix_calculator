﻿using Matrix_Calculator.API.Models;
using Matrix_Calculator.Base;
using Matrix_Calculator.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Matrix_Calculator.API.Controllers
{
    [Route("[api]/[controller]/[action]")]
    [ApiController]
    public class MatrixController : ControllerBase
    {
        private MatrixServices _matrixServices;
        public MatrixController(MatrixServices matrixServices)
        {
            _matrixServices = matrixServices;
        }

        [HttpPost]
        public Matrix Transpose([FromBody] JObject jObject)
        {
            double[,] elements = jObject.Value<JArray>("matrix").ToObject<double[,]>();
            Matrix matrix = new Matrix(elements);

            return _matrixServices.Transpose(matrix);
        }

        [HttpPost]
        public ActionResponse<Matrix> ScalarMultiply([FromBody] JObject jObject)
        {
            ActionResponse<Matrix> actionResponse = new ActionResponse<Matrix>();

            double lambda = jObject.Value<double>("lambda");

            double[,] elements = jObject.Value<JArray>("matrix").ToObject<double[,]>();
            Matrix matrix = new Matrix(elements);

            try
            {
                Matrix matrix2 =_matrixServices.ScalarMultiply(lambda, matrix);
                actionResponse.Data = matrix2;
                actionResponse.Success = true;
            }
            catch (Exception exception)
            {
                actionResponse.Success = false;
                actionResponse.Message = exception.Message;
            }

            return actionResponse;
        }
    }
}
