﻿using System;

namespace Matrix_Calculator.Base
{
    public class Matrix
    {
        #region Properties
        public int RowCount { get => Elements.GetLength(0); }
        public int ColumnCount { get => Elements.GetLength(1); }
        public double[,] Elements { get; set; }
        #endregion

        #region Constructors
        public Matrix(int rowCount, int columnCount)
        {
            if (rowCount <= 0 || columnCount <= 0) { throw new Exception("Matrix dimensions have to be greater than zero"); }
            this.Elements = new double[rowCount, columnCount];
        }
        public Matrix(double[,] elements)
        {

            this.Elements = (double[,])elements.Clone();
        }
        #endregion

    }
}
